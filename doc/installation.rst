############
INSTALLATION
############

This tutorial is intended to Unix users.
However, it should be easy for window users to make it work.

Quadproj can be installed with `conda`, `pip`, or :ref:`from sources <installation-from-source>`.

************************
How to install quadproj?
************************

Quick Install
=============

Install the package *via* `pip <https://pypi.org/project/quadproj/>`_, *e.g.*,

>>> python3 -m pip install quadproj

Do not forget the requirements:

>>> python3 -m pip install numpy scipy matplotlib imageio pytest

.. note::
        Depending on your OS, the python alias may be instead: python, py, py3.

You can also install the package *via* `conda <https://anaconda.org/loicvh/quadproj>`_ from `loicvh` channel:

>>> conda install -c loicvh quadproj 

And install the requirements:

>>> conda install numpy scipy matplotlib imageio pytest

Installation on a virtual environment
=====================================

To ensure reproducibility and avoid any dependence issue, it is a better practice to run any project inside a dedicated environment.

This is possible using `pip` or `conda`.

PIP
***

Ensure that you have `venv` (or install it with `pip`) and create a virtual environment.

>>> python3 -m venv quadproj-env

Activate it.

>>> source quadproj-env/bin/activate

Install quadproj:

>>> python3 -m pip install quadproj

Download and install the requirements_.

>>> wget https://gitlab.com/quadproj_package/quadproj/-/blob/master/requirements.txt
>>> python3 -m pip install -r requirements.txt


CONDA
*****


Conda users may instead use (after downloading the requirements_):

.. code-block:: bash

        conda create -n quadproj-env
        conda activate quadproj-env
        # Install quadproj from loicvh's channel
        conda install -c loicvh quadproj
        # Install the requirements
        conda install --file requirements.txt


.. _installation-from-source:

Installation from source
========================

Clone the repository:

>>> git clone https://gitlab.com/quadproj_package/quadproj.git

Ensure that you have `venv` (or install it with pip/conda) and create a virtual environment:

>>> python3 -m venv quadproj-env

Activate it.

>>> source quadproj-env/bin/activate

... and install the requirements.

>>> cd quadproj
>>> python3 -m pip install -r requirements.txt

Then you can either install the last release of `quadproj`

>>> python3 -m pip install quadproj

**or** you may install in developer mode

>>> python3 -m pip install -e .

You can now run the tests, *e.g.*, using `pytest`.

>>> pytest tests


.. _requirements: https://gitlab.com/quadproj_package/quadproj/-/blob/master/requirements.txt
