.. Quadproj documentation master file, created by
   sphinx-quickstart on Tue May  5 11:56:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

quadproj documentation
=============================================================

**Date**: |today| **Version**: |version|

**Useful links**:
`Pypi page <https://pypi.org/project/quadproj/>`__ |
`Source Repository <https://gitlab.com/quadproj_package/quadproj>`__ |
`Issues & Ideas <https://gitlab.com/quadproj_package/quadproj/issues>`__

**quadproj** is an open-source software for projecting onto quadrics (*i.e.*, quadratic surfaces).

.. image:: ../output/one_sheet_hyperboloid_circle.gif


.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   
   installation
   quickstart
   modules


