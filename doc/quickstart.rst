Quadproj quickstart
===================

This page gives you a quick overview of the use of `quadproj`. 

The following code snippets should be run sequentially (*e.g.*, to inherit from `import`), the whole script is available in test_README_.

.. _test_README: https://gitlab.com/quadproj_package/quadproj/-/blob/master/tests/test_README.py

The basics
----------

A simple N dimensional example
******************************

In the following snippet, we create an object `quadproj.quadrics.Quadric`.
A quadric is a quadratic hypersurface of the form

.. math:: \mathbf{x}^t A \mathbf{x} + \mathbf{b}^t \mathbf{x} +c = 0.

It is created by providing a `dict` containing the matrix `A`, the vector `b` and the scalar `c`.
After definition of the latter parameters, the object is created in a single line (line 17).

We then create some point `x0` that we project onto the quadric.

.. literalinclude:: tests/basics1.py
   :language: python3
   :linenos:

For data of dimension larger than three, it is unfortunately rather difficult to plot the quadric.

Visualise the solution
**********************

Let us try a simpler 2D case.

.. literalinclude:: tests/basics2.py
   :language: python3
   :linenos:

The output is given in `output/ellipse_no_circle.png` and the projection of `x0` onto the quadric (`x_project`) is depicted as a red point.
Mathematically we write the projection :math:`\mathrm{P}_\mathcal{Q}(x^0)`.


.. image:: ../output/ellipse_no_circle.png

Remark that you can obviously change the output to the current directory (or whatever path).

>>> plt.savefig('ellipse_no_circle.png')

Looking at the previous image, one may believe that the returned projection (red point) is actually **not** the closest point to 
`x0`. This *false* impression is due to the unequal axes.
As a way to verify that the projection is indeed the closest point, we run the following snippet.

.. literalinclude:: tests/basics3.py
   :language: python3
   :linenos:

Switching the parameter `flag_circle` to `True`, we verify the optimality of `x_project`.

.. image:: ../output/ellipse_circle.png

Indeed, we observe that the circle centered in `x0` with radius :math:`\| \mathbf{x} - \mathbf{x}^0 \|` is:

* tangent at `x_project` = :math:`\| \mathrm{P}_{\mathcal{Q}} (\mathbf{x}^0) \|_2` ;
* does not cross the quadric.

Quid of multiple solutions?
***************************

The projection is not unique in general! We denote as *degenerate case* the cases where `x0` is located on one of the principal axes of the quadric.

In this degenerate case, it is possible (though not always the case) that multiple points are optima.

For constructing a degenerate case we can:

- Either construct a quadric in standard form, *i.e.*, with a diagonal matrix `A`, a nul vector `b`, and `c=-1` and define `x0` with a least one entry equal to zero;
- Or choose any quadric and select `x0` to be on any axis.


Let us illustrate the second option. We create `x0` by applying the (inverse) standardization from some `x0` with at least one entry equal to zero.

Here, we chose to be close to the center and on the longest axis of the ellipse so as to be sure that there are multiple (two) solutions.

Note that the program returns **only one solution**.
Multiple solutions is planned in future releases.

.. literalinclude:: tests/basics4.py
   :language: python3
   :linenos:

The output figure `ellipse_degenerated.png` is given below.
It can be shown that the reflection of the `x_project` along the largest ellipse axis (visible because `show_principal_axes=True`) yields another optimal solution.


.. image:: ../output/ellipse_degenerated.png

Supported quadrics
------------------

Ellipse
********
See previous section for examples of ellipses.

Hyperbola
*********

We illustrate a degenerated projection onto an hyperbola.

In this case, there is no root to the nonlinear function (graphically, the second axis does not intersect the hyperbola). This is not an issue because two solutions are obtained from the other set of KKT points (:ref:`How does quadproj works?<How does quadproj works?>`).

.. literalinclude:: tests/basics5.py
   :language: python3
   :linenos:

.. image:: ../output/hyperbola_degenerated.png


Ellipsoid
*********

Similarly as the 2D case, we can plot ellipsoid.

.. image:: ../output/ellipsoid.png

.. literalinclude:: tests/basics6.py
   :language: python3
   :linenos:


To ease visualisation, the function `get_turning_gif` lets you write a rotating gif such as:

.. image:: ../output/ellipsoid.gif

One-sheet hyperboloid
*********************

You can easily create a turning gif of a modification of the plots (*e.g.,* by adding the projected point) by having a look at the core of `get_turning_gif`.

.. literalinclude:: tests/basics7.py
   :language: python3
   :linenos:

.. image:: ../output/one_sheet_hyperboloid.gif

But wait! It seems that the red point is **not** the closest isn't it?!

Fortunately, this is not a bug but rather the same scaling issue from :ref:`Visualise the solution<Visualise the solution>` and plotting the iso-curve of the distance function (`flag_circle=True`), we see that the red ellipsoid (*i.e.,* a distorted ball) is tangent to the quadric at `x_project`.

.. literalinclude:: tests/basics8.py
   :language: python3
   :linenos:

.. image:: ../output/one_sheet_hyperboloid_circle.gif

Two-sheet hyperboloid
*********************

A Two-sheet hyperboloid is a 3D quadratic surface with two positive eigenvalues and one negative.

.. literalinclude:: tests/basics9.py
   :language: python3
   :linenos:

.. image:: ../output/two_sheet_hyperboloid.gif
