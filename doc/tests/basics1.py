from quadproj import quadrics
from quadproj.project import project


import numpy as np


# creating random data
dim = 42
_A = np.random.rand(dim, dim)
A = _A + _A.T  # make sure that A is positive definite
b = np.random.rand(dim)
c = -1.42


param = {'A': A, 'b': b, 'c': c}
Q = quadrics.Quadric(param)

x0 = np.random.rand(dim)
x_project = project(Q, x0)
assert Q.is_feasible(x_project), 'The projection is incorrect!'
