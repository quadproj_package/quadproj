A[0, 0] = 4
A[1, 1] = -2
A[2, 2] = -1

b = np.array([0.5, 1, -0.25])

c = -1

param = {'A': A, 'b': b, 'c': c}
Q = quadrics.Quadric(param)

x0 = np.array([0.1, 0.42, -0.45])

x_project = project(Q, x0)

fig, ax = Q.plot()
ax.grid(False)
ax.axis('off')
plot_x0_x_project(ax, Q, x0, x_project)

if save_gif:
    quadrics.get_gif(fig, ax, gif_path=join(output_folder, Q.type+'.gif'))
if show:
    plt.show()
