fig, ax = Q.plot()
ax.grid(False)
ax.axis('off')
plot_x0_x_project(ax, Q, x0, x_project, flag_circle=True)
ax.get_legend().remove()

if save_gif: 
    quadrics.get_gif(fig, ax, elev=15, gif_path=join(output_folder, Q.type+'_circle.gif'))
