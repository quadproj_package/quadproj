

A[0, 0] = -4

param = {'A': A, 'b': b, 'c': c}
Q = quadrics.Quadric(param)

x0 = np.array([0.1, 0.42, -1.5])

x_project = project(Q, x0)

fig, ax = Q.plot()
ax.grid(False)
ax.axis('off')
plot_x0_x_project(ax, Q, x0, x_project)
ax.get_legend().remove()

save_gif = True
if save_gif:
    quadrics.get_gif(fig, ax, elev=15, gif_path=join(output_folder, Q.type+'.gif'))
if show:
    plt.show()
