dim = 3
A = np.eye(dim)
A[0, 0] = 2
A[1, 1] = 0.5

b = np.zeros(dim)
c = -1
param = {'A': A, 'b': b, 'c': c}
Q = quadrics.Quadric(param)


fig, ax = Q.plot()

fig.savefig(join(output_folder, 'ellipsoid.png'))

Q.get_turning_gif(step=4, gif_path=join(output_folder, Q.type+'.gif'))
