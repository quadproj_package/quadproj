from quadproj.project import plot_x0_x_project
import quadproj.utils
from os.path import join

import pathlib


root_folder = pathlib.Path(__file__).resolve().parent.parent
output_folder = join(root_folder, 'output')

import matplotlib.pyplot as plt

show=False

A = np.array([[1, 0.1], [0.1, 2]])
b = np.zeros(2)
c = -1
Q = quadrics.Quadric({'A': A, 'b': b, 'c': c})

x0 = np.array([2, 1])
x_project = project(Q, x0)

fig, ax = Q.plot(show=show)
plot_x0_x_project(ax, Q, x0, x_project)
plt.savefig(join(output_folder, 'ellipse_no_circle.png'))
